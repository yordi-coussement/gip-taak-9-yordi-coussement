# Taak 9 Javascript
Yordi Coussement


## Wat houdt de taak in?
Bij de taak maak ik een wetenschappelijk Javascript toepassing. De bedoeling is dat het gebruik maakt van animaties of een API.

# Ontwerp Javascript-tool

Als idee voor de tool had ik gedacht aan een geanimeerde delay-calculator. Zoals iedereen op een fuif al wel gemerkt heeft, is er verschil tussen het geluid vanvoor en het geluid achteraan waar er nog enkele luidsprekers staan. Dit probleem valt op te lossen door delay te zetten op de luidsprekers achteraan. Hierdoor zal het geluid samenlopen.

Uiteraard is het iets dat niet makkelijk is omdat er een rekensommetje aan verbonden is. In mijn pagina wil ik deze calculator verwerken met animerende zaken.

De gebruiker kan zijn afstand ingeven in een input vak.

Wat is er nu wetenschappelijk aan deze toepassing? Het feit dat geluid zich in golven voortbeweegd en ervoor zorgt dat er verschil zit op de tijd tussen verschillende afstanden. Dit kan enkel opgelost worden met door vertraging te zetten op luidsprekers zodat deze samenlopen.

